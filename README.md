# VueDoc Addon for Storybook.js

This is a VueDoc addon for Storybook.js.
It uses @vuedoc/parser (through vuedoc-loader) to parse JSDoc comments in your `.vue` files
and show them in a VueDoc panel in your Storybook

## Installation

` $ npm install --save-dev @storybook/addon-vuedoc vuedoc-loader @vuedoc/parser`

In your `addons.js` file add:

`import '@storybook/addon-vuedoc/register';`

## Usage

In your story file, you will need to add the `withVuedoc` decorator to the story.
Then you will need to import the VueDocs through `vuedoc-loader`
and pass them with the `vuedoc` option to the component's story.

```js

import { withVuedoc } from '@storybook/addon-vuedoc';

import MyComponent from './components/MyComponent.vue';
import vuedocMyComponent from '!!vuedoc-loader!./components/MyComponent.vue';

storiesOf('my component', module)
  // ...
  .addDecorator(withVuedoc)
  .add('my component example', () => ({
    render(h) {
      return h(MyComponent);
    },
  }), {
    vuedoc: vuedocMyComponent
  })
```

The `!!vuedoc-loader!...` import syntax is necessary because
we don't want to change the normal loading behavior of `.vue` files (so we don't add it to our webpack configuration),
and we don't want the normal loading behavior to affect `vuedoc-loader`.
The single `!` tells webpack to use `vuedoc-loader` and the `!!` tells it to ignore any other defined loaders.

Any resolve tweaks you have in place (resolve roots, aliases, etc) will work as expected.
