import addons, { makeDecorator } from '@storybook/addons';

export const withVuedoc = makeDecorator({
  name: 'withVuedoc',
  parameterName: 'vuedoc',
  skipIfNoParametersOrOptions: true,
  wrapper: (getStory, context, { parameters: docs }) => {
    const channel = addons.getChannel();

    channel.emit('addon-vuedoc/set_vuedoc', docs);

    return getStory(context);
  }
})
