import React from 'react';
import styled from '@emotion/styled';

import { componentHeading, sectionHeading, separator } from './styles';
import VuedocTable from './table';

const e = React.createElement;

const PROPS_TABLE_COLUMNS = [
  { key: 'name', label: 'Name' },
  { key: 'value.type', label: 'Type' },
  { key: 'value.required', label: 'Required', render: val => val ? 'YES' : 'NO' },
  { key: 'value.default', label: 'Default' },
  { key: 'description', label: 'Description', weight: 1 },
];

const EVENTS_TABLE_COLUMNS = [
  { key: 'name', label: 'Name' },
  { key: 'description', label: 'Description', weight: 1 },
];

const SLOTS_TABLE_COLUMNS = [
  { key: 'name', label: 'Name' },
  { key: 'description', label: 'Description', weight: 1 },
];

export default class Vuedoc extends React.Component {
  constructor(props) {
    super(props);

    this.stopListeningOnStory = () => {};
    this.isUnmounted = false;

    this.state = {
      docs: null,
    };

    this.onSetVuedoc = this.onSetVuedoc.bind(this);
  }

  onSetVuedoc(docs) {
    this.setState({ docs });
  }

  componentDidMount() {
    const { channel, api } = this.props;
    channel.on('addon-vuedoc/set_vuedoc', this.onSetVuedoc);

    this.stopListeningOnStory = api.onStory(() => {
      this.onSetVuedoc(null);
    });
  }

  render() {
    const { active } = this.props;
    const { docs } = this.state;

    if (!active || !docs) {
      return null;
    }

    const panelContents = [
      e(componentHeading, null, `Component: ${docs.name}`),
      e('p', null, docs.description),

      e(sectionHeading, null, 'Props'),
      e(VuedocTable, { columns: PROPS_TABLE_COLUMNS, data: docs.props }),

      e(separator),

      e(sectionHeading, null, 'Events'),
      e(VuedocTable, { columns: EVENTS_TABLE_COLUMNS, data: docs.events }),

      e(separator),

      e(sectionHeading, null, 'Slots'),
      e(VuedocTable, { columns: SLOTS_TABLE_COLUMNS, data: docs.slots }),
    ];

    return e(styled.div({
      margin: 10,
      overflow: 'auto',
    }), {}, ...panelContents);
  }

  componentWillUnmount() {
    this.stopListeningOnStory();

    this.isUnmounted = true;
    const { channel } = this.props;
    channel.removeListener('addon-vuedoc/set_vuedoc', this.onSetVuedoc);
  }
}
