import styled from '@emotion/styled';

export const componentHeading = styled.h2({
  'font-size': '24px',
  'margin': '.25em 0',
})

export const sectionHeading = styled.h4({
  'font-size': '20px',
  'margin': '.5em 0 .25em 0',
});

export const separator = styled.hr({
  'border': '0',
  'border-bottom': 'solid 1px #dcdcdc',
});

export const table = styled.table({
  'width': '100%',
});

export const tableHead = styled.th({
  'border-bottom': 'solid 1px #dcdcdc',
});

export const tableCell = styled.td({
  'padding': '.25em .5em',
});

