import React from 'react';
import dot from 'dot-prop';

import { table, tableHead, tableCell } from './styles';

const e = React.createElement;

export default class VuedocTable extends React.Component {
  render() {
    const { columns, data } = this.props;

    const columnWidths = `${(100 / columns.length).toFixed(3)}%`;

    const columnHeadings = columns.map(col => e(tableHead, { width: columnWidths }, col.label));

    const rows = data.map(row => {
      const cells = columns.map(col => {
        const value = dot.get(row, col.key);

        return e(tableCell, null, col.render ? col.render(value) : value)
      })

      return e('tr', null, ...cells);
    })

    return e(table, {},
      e('thead', null, e('tr', {}, ...columnHeadings)),
      e('tbody', null, ...rows),
    );
  }
}
