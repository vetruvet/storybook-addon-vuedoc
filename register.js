import React from 'react';
import addons from '@storybook/addons';

import VuedocPanel from './components/vuedoc-panel';

addons.register('addon-vuedoc', (api) => {
  const channel = addons.getChannel();

  addons.addPanel('addon-vuedoc/panel', {
    title: 'VueDoc',
    render: ({ active }) => React.createElement(VuedocPanel, { channel, api, active }, null),
  });
});
